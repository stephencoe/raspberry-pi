# PI IR Remote

Only configured with the power button.


## Step 1: Build the circuit
[sunfounder](https://www.sunfounder.com/learn/sensor-kit-v2-0-for-raspberry-pi-b-plus/lesson-23-ir-remote-control-sensor-kit-v2-0-for-b-plus.html)

## Step 2: Download the LIRC library
    `sudo apt-get install lirc`
## Step 3: Set up lirc:
Open your /etc/modules file:
`sudo vim /etc/modules`
Add this to the end:
```
lirc_dev
lirc_rpi gpio_in_pin=18
```

Edit the /etc/lirc/hardware.conf file:
`sudo vim /etc/lirc/hardware.conf`
Modify the file as shown below:
```
# /etc/lirc/hardware.conf
#
# Arguments which will be used when launching lircd
LIRCD_ARGS="--uinput"

# Don't start lircmd even if there seems to be a good config file
# START_LIRCMD=false

# Don't start irexec, even if a good config file seems to exist.
# START_IREXEC=false

# Try to load appropriate kernel modules
LOAD_MODULES=true

# Run "lircd --driver=help" for a list of supported drivers.
DRIVER="default"
# usually /dev/lirc0 is the correct setting for systems using udev
DEVICE="/dev/lirc0"
MODULES="lirc_rpi"

# Default configuration files for your hardware if any
LIRCD_CONF=""
LIRCMD_CONF=""
```
Copy the remote configuration file lircd.conf to/home/pi and /etc/lirc:
`cp ./remotes/lircd.conf /home/pi`

`sudo cp lircd.conf /etc/lirc/`
Open the /boot/config.txt file:
sudo vim /boot/config.txt
Add the following line to the end:
dtoverlay=lirc-rpi:gpio_in_pin=23,gpio_out_pin=22
Press Ctrl +O and Ctrl +X, save and exit .
Reboot the Raspberry Pi after the change.
sudo reboot
## Step 4: Test the IR receiver
Check if lirc module is loaded:
`ls /dev/li*`
You should see this:
`/dev/lirc0      /dev/lircd`
Run the command to stop lircd and start outputting raw data from the IR receiver:
`irw`
When you press a button on the remote, you can see the button name printed on the screen.
```
pi@raspberrypi:~ $ irw
0000000000000001 00 KEY_POWER /home/pi/lircd.conf
```
If it does not appear, somewhere may be incorrectly configured. Check again that you’ve connected everything and haven’t crossed any wires.

For C language users:

## Step 5: Download LIRC client library:
sudo apt-get install liblircclient-dev

## Step 6: Change directory
 cd /home/pi/SunFounder_SensorKit_for_RPi2/C/23_ircontrol/

## Step 7: Create a lirc directory under /etc/lirc/:
sudo mkdir /etc/lirc/lirc/
Copy the lircrc file to /etc/lirc/lirc/:
sudo cp lircrc /etc/lirc/lirc/

## Step 8: Compile
gcc ircontrol.c -lwiringPi -llirc_client

## Step 9: Run
sudo ./a.out
For Python users:

## Step 5: Download pylirc:
sudo apt-get install python-pylirc

## Step 6: Change directory:
cd /home/pi/SunFounder_SensorKit_for_RPi2/Python/

## Step 7: Run
sudo python 23_ircontrol.py


## Send the action
`irsend SEND_ONCE Samsung KEY_POWER`

## Recording new buttons
```
sudo /etc/init.d/lirc stop
irrecord -f -d /dev/lirc0 ~/lirc.conf
sudo /etc/init.d/lirc start
```
